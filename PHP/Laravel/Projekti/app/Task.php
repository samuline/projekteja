<?php namespace teht;

use Illuminate\Database\Eloquent\Model;

class Task extends Model {

    protected $guarded = [];

    public function project()
    {
        return $this->belongsTo('teht\Project');
    }

}
