<?php namespace teht;

use Illuminate\Database\Eloquent\Model;

class Tickets extends Model {

	protected $guarded = [];
	public function comments()
	{
		return $this->hasMany('teht\Comments','on_post');
	}
	
	public function author()
	{
		return $this->belongsTo('teht\User','author_id');
	}

}
