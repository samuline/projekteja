<?php namespace teht;

use Illuminate\Database\Eloquent\Model;

class Comments extends Model {

	protected $guarded = [];
	
	
	public function author()
	{
		return $this->belongsTo('teht\User','from_user');
	}
	
	public function post()
	{
		return $this->belongsTo('teht\Posts','on_post');
	}

}
