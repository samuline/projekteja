<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
/*
Route::get('/', [
    'uses' => '\teht\Http\Controllers\HomeController@index',
    'as'   => 'index'
]);

Route::get('/auth/register', [
    'uses' => '\teht\Http\Controllers\AuthController@getRegister',
    'as'   => 'auth.register',
    'middleware' => ['guest']
]);
 
Route::post('/auth/register', [
    'uses' => '\teht\Http\Controllers\AuthController@postRegister',
    'middleware' => ['guest']
]);
 
Route::get('/auth/signin', [
    'uses' => '\teht\Http\Controllers\AuthController@getLogin',
    'as'   => 'auth.login',
    'middleware' => ['guest']
]);
 
Route::post('/auth/signin', [
    'uses' => '\teht\Http\Controllers\AuthController@postLogIn',
    'middleware' => ['guest']
]);
 
Route::get('/logout', [
    'uses' => '\teht\Http\Controllers\AuthController@logOut',
    'as'   => 'auth.logout'
]);

Route::resource('projects', 'ProjectController');

Route::post('projects/{projects}/tasks', [
    'uses' => '\teht\Http\Controllers\ProjectTasksController@postNewTask',
    'as' => 'projects.tasks.create'
]);

Route::get('projects/{projects}/tasks/{tasks}/edit', [
    'uses' => '\teht\Http\Controllers\ProjectTasksController@getOneProjectTask',
    'as' => 'projects.tasks'
]);
 
Route::put('projects/{projects}/tasks/{tasks}', [
    'uses' => '\teht\Http\Controllers\ProjectTasksController@updateOneProjectTask',
]);

Route::delete('projects/{projects}/tasks/{tasks}', [
    'uses' => '\teht\Http\Controllers\ProjectTasksController@deleteOneProjectTask',
]);

Route::post('projects/{projects}/files', [
     'uses' => '\teht\Http\Controllers\FilesController@uploadAttachments',
     'as'   => 'projects.files',
     'middleware' => ['auth']
]);

Route::post('projects/{projects}/comments', [
    'uses' => 'ProjectCommentsController@postNewComment',
    'as'   => 'projects.comments.create',
    'middleware' => ['auth']
]);

Route::get('projects/{projects}/comments/{comments}/edit', [
    'uses' => 'ProjectCommentsController@getOneProjectComment',
    'as' => 'projects.comments'
]);

Route::put('projects/{projects}/comments/{comments}', [
    'uses' => 'ProjectCommentsController@updateOneProjectComment',
]);

Route::delete('projects/{projects}/comments/{comments}', [
    'uses' => 'ProjectCommentsController@deleteOneProjectComment',
]);

Route::post('projects/{projects}/collaborators', [
    'uses' => 'ProjectCollaboratorsController@addCollaborator',
    'as'   => 'projects.collaborators.create',
    'middleware' => ['auth']
]);
*/


Route::get('/',['as' => 'home', 'uses' => 'ProjectsController@index']);

Route::get('/home',['as' => 'home', 'uses' => 'ProjectsController@index']);
Route::get('search', array(
     'as'    =>  'search',
     'uses'  =>  'SearchController@index'
 ));

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);


Route::group(['middleware' => ['auth']], function()
{
	Route::model('tasks', 'Task');
	Route::model('projects', 'Project');

	Route::bind('tasks', function($value, $route) {
	return teht\Task::whereSlug($value)->first();
	});
	Route::bind('projects', function($value, $route) {
	return teht\Project::whereSlug($value)->first();
	});

	Route::resource('projects', 'ProjectsController');
	Route::resource('projects.tasks', 'TasksController');
	Route::get('create','ProjectsController@create');
	// show new ticket form
	Route::get('new-ticket','PostController@create');
	//show all tickets
	Route::get('/tickets', 'PostController@index');

	// save new ticket
	Route::post('new-ticket','PostController@store');
	
	// edit ticket form
	Route::get('edit/{slug}','PostController@edit');
	
	// update ticket
	Route::post('update','PostController@update');
	
	// delete ticket
	Route::get('delete/{id}','PostController@destroy');
	
	// display user's all tickets
	Route::get('my-all-posts','UserController@user_posts_all');
	
	// display user's drafts
	Route::get('my-drafts','UserController@user_posts_draft');
	
	
	// add comment
	Route::post('comment/add','CommentController@store');
	
	// delete comment
	Route::post('comment/delete/{id}','CommentController@destroy');
	
});

//users profile
Route::get('user/{id}','UserController@profile')->where('id', '[0-9]+');

// display list of tickets
Route::get('user/{id}/posts','UserController@user_posts')->where('id', '[0-9]+');

// display single ticket
Route::get('/{slug}',['as' => 'post', 'uses' => 'PostController@show'])->where('slug', '[A-Za-z0-9-_]+');

