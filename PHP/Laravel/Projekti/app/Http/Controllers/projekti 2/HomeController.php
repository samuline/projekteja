<?php
 
namespace teht\Http\Controllers;
 
use Illuminate\Http\Request;
 
use teht\Http\Requests;
use teht\Http\Controllers\Controller;
 
class HomeController extends Controller
{
    /**
     * Displays the index page of the app
     *
     * @return Response
     */
    public function index()
    {
        return view('index');
    }
}
