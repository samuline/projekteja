<?php
namespace teht\Http\Controllers;

use teht\Tickets;
use teht\Http\Requests;
use teht\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use teht\Comments;
use teht\Posts;
use Redirect;
use Illuminate\Http\Request;

 class SearchController extends Controller {

     public function index()
        {
            if (Input::has('query')) {
                 $query = Input::get('query');

             $results = Posts::whereRaw("MATCH(title) AGAINST(? IN BOOLEAN MODE)", array($query))->orderBy('id','ASC')->get();

             if ($results->count() > 0) {
                 return View::make('search.main')->with('results',$results)->with('query', $query)->with('title', 'Search Result for '.$query);
             } else {
                 return '<h3>Sorry, No results for ' . $query . ' </h3>';
             }
         }

         return Redirect::to('/');
     }
}
