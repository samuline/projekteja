@extends('app')
@section('title')
<h2>Create Task for Project "{{ $project->name }}"</h2>
@endSection
@section('content')
    

    {!! Form::model(new teht\Task, ['route' => ['projects.tasks.store', $project->slug], 'class'=>'']) !!}
        @include('tasks/partials/_form', ['submit_text' => 'Create Task'])
    {!! Form::close() !!}
@endsection
