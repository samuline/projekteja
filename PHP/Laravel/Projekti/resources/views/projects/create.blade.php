@extends('app')
@section('title')
<h2>Create Project</h2>
@endSection
@section('content')
    

    {!! Form::model(new teht\Project, ['route' => ['projects.store']]) !!}
        @include('projects/partials/_form', ['submit_text' => 'Create Project'])
    {!! Form::close() !!}
@endsection
