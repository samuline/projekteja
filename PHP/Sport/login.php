<?php 
include_once("dbconfig.php");
session_start();

if($_SERVER["REQUEST_METHOD"] == "POST")
 {
// username and password received from loginform 
$username=mysqli_real_escape_string($dbconfig,$_POST['username']);
$password=mysqli_real_escape_string($dbconfig,$_POST['password']);

$sql="SELECT * FROM Kayttajat WHERE Tunnus= :username;";
$sql = $dbh->prepare($sql);
$sql->bindParam(":username", $username, PDO::PARAM_STR);
$ok = $sql->execute();
$h=0;
while($row = $sql->fetch(PDO::FETCH_ASSOC) ) {
	foreach($row as $key => $value ) {
		if($h==2){
	//echo "<br>".$value;
	$pass = $value;
	$h=0;
	}else{$h++;}
	}
}
if($pass === crypt($password, $pass)){

$_SESSION['login_user']=$username;
header("location: welcome-home.php");

if($_COOKIE["napit"] == "haku") { 
	header("location: haku.php");
	
	}
else if($_COOKIE["napit"] =="lisaa"){
	header("location: lisaa.php");
}
unset($_COOKIE["napit"]);
$res = setcookie("napit",null,time()-3600);

}
else
{
echo '<script type="text/javascript">alert(""Username or Password is invalid"!");</script>';
}
}
$image5_url="logo3_6.png";
$image_url="3d.png";
 $image_url2="3d_2.png";
 $image_url3="3d_3.png";
?>
<!DOCTYPE html>
<html>
<head>
<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<meta charset="UTF-8" name="viewport" content="initial-scale=1.0",width=device-width"/>
<link rel="stylesheet" type="text/css" href="style.css">
<script type="text/javascript">
window.setTimeout("location=('logout.php');",600000);
</script>
<title>Testitulokset</title>

</head>

<body>

<div class="logo"><img src="<?php echo $image5_url?>"></div>
<div class="login-block">
	<div class="Etu" align="center">
    <h1>Kirjaudu</h1></div>
    <form method="post" action="" name="loginform">
    <input type="text"  placeholder="Username" id="username" name="username" />
    <input type="password"  placeholder="Password" id="password" name="password" />
    <button type="submit" class="button" id="kirjaudu">Kirjaudu</button>
    </form>
</div>
</body>

</html>