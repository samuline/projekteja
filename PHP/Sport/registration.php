 <?php
include_once("dbconfig.php");
if($_SERVER["REQUEST_METHOD"] == "POST")
 {
// username and password received from loginform 
$username=mysqli_real_escape_string($dbconfig,$_POST['username']);
$password=mysqli_real_escape_string($dbconfig,$_POST['password']);
$bob = false;
$sql="SELECT * FROM Kayttajat WHERE Tunnus= :username;";
$sql = $dbh->prepare($sql);
$sql->bindParam(":username", $username, PDO::PARAM_STR);
$ok = $sql->execute();

while($row = $sql->fetch(PDO::FETCH_ASSOC)) {
	foreach($row as $key => $value ) {
		if($value == $username){
			$bob = true;
		}
		
	}
}
if(!$bob){
$salt = openssl_random_pseudo_bytes(128);
$salt = '$6$rounds=5000$' . strtr(base64_encode($salt), array('_' => '.', '~' => '/'));
$password_hash = crypt($password, $salt);

if(!empty($_POST['username']) && !empty($_POST['password'])){
$sql="Insert into Kayttajat(Tunnus,Salasana) values('$username','$password_hash')";
$result=mysqli_query($dbconfig,$sql);
echo '<script type="text/javascript">alert("Registeration succesfull!");</script>';
}else{
echo '<script type="text/javascript">alert("Check username and password!");</script>';
}
 }else{
	echo '<script type="text/javascript">alert("Käyttäjä on jo olemassa");</script>';
 }
 }
?>
<!DOCTYPE html>
<html>
<head>
<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<meta charset="UTF-8">
<title>Registeration</title>
<style>
body {
    background-size: cover;
    font-family: Montserrat;
}

.logo {
    width: 213px;
    height: 36px;
    margin: 30px auto;
}

.login-block {
    width: 320px;
    padding: 20px;
    background: #fff;
    border-radius: 5px;
    border-top: 5px solid #005610;
    margin: 0 auto;
}

.login-block h1 {
    text-align: center;
    color: #000;
    font-size: 18px;
    text-transform: uppercase;
    margin-top: 0;
    margin-bottom: 20px;
}

.login-block input {
    width: 100%;
    height: 42px;
    box-sizing: border-box;
    border-radius: 5px;
    border: 1px solid #ccc;
    margin-bottom: 20px;
    font-size: 14px;
    font-family: Montserrat;
    padding: 0 20px 0 50px;
    outline: none;
}

.login-block input#username {
    background: #fff url('http://i.imgur.com/u0XmBmv.png') 20px top no-repeat;
    background-size: 16px 80px;
}

.login-block input#username:focus {
    background: #fff url('http://i.imgur.com/u0XmBmv.png') 20px bottom no-repeat;
    background-size: 16px 80px;
}

.login-block input#password {
    background: #fff url('http://i.imgur.com/Qf83FTt.png') 20px top no-repeat;
    background-size: 16px 80px;
}

.login-block input#password:focus {
    background: #fff url('http://i.imgur.com/Qf83FTt.png') 20px bottom no-repeat;
    background-size: 16px 80px;
}

.login-block input:active, .login-block input:focus {
    border: 1px solid #1FDD0E;
}

.login-block button {
    width: 100%;
    height: 40px;
    background: #005610;
    box-sizing: border-box;
    border-radius: 5px;
    border: 1px solid #00350A;
    color: #fff;
    font-weight: bold;
    text-transform: uppercase;
    font-size: 14px;
    font-family: Montserrat;
    outline: none;
    cursor: pointer;
}

.login-block button:hover {
    background: #1FDD0E;
}

</style>
</head>

<body>

<div class="logo"></div>
<div class="login-block">
  <?php if(@$msg){ echo $msg;}?> 
<h1>Registration</h1>
    <form method="post" action="" name="loginform">
    <input type="text" placeholder="Username" id="username" name="username"</>
    <input type="password" placeholder="Password" id="password" name="password" />
    <button type="submit">Submit</button>
    </form>
<?php if(@$msg){?><a href="login.php">Login</a><?php }?> 
</div>
</body>

</html>