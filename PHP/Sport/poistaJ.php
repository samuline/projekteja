<?php
include("dbconfig.php");

if ($dbconfig->connect_error)
{
    die('dbconfignect Error (' . $dbconfig->connect_errno . ') ' . $dbconfig->connect_error);
}

$sql = "DELETE FROM jaa_testi WHERE numero = ?";
if (!$result = $dbconfig->prepare($sql))
{
    die('Query failed: (' . $dbconfig->errno . ') ' . $dbconfig->error);
}

if (!$result->bind_param('i', $_GET['id']))
{
    die('Binding parameters failed: (' . $result->errno . ') ' . $result->error);
}

if (!$result->execute())
{
    die('Execute failed: (' . $result->errno . ') ' . $result->error);
}

if ($result->affected_rows > 0)
{
	echo '<script type="text/javascript">alert("Testi poistettu!");</script>';
    header("location: haku.php");
}
else
{
	echo '<script type="text/javascript">alert("Ei onnistunut!");</script>';
    header("location: haku.php");
}
$result->close();
$dbconfig->close();

?>