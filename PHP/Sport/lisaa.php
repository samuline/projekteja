<?php 
if(empty($_SERVER["HTTPS"]) || $_SERVER["HTTPS"] !== "on"){
	$url = "https://".$_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
	header("Location: $url");
	exit();
}else {
	
}
include("dbconfig.php");
session_start();
if(!isset($_SESSION['login_user']))
	{
	header("Location: login.php");
	}
	if($_REQUEST["napit"] == "etusivu")
 {

	header("location: welcome-home.php");
 }
 else if($_REQUEST["napit"] == "haku")
 {	

	header("location: haku.php");
 }
 else if($_REQUEST["napit"] == "logout")
 {
	header("location: logout.php");
 }
?>

<?php 
function pvmtark($pvm){

		$pvm = str_replace('/','.', $pvm);
		$pvm = str_replace('-','.', $pvm);
		
		if($pvm[1] == '.'){
			$osa = "0";
			$osa = $osa . $pvm;
			if($osa[4] == '.'){
				$osa = substr($osa,0,2).(".0" . substr($osa,3,strlen($osa)));
			}
			$pvm = $osa;
			
		}	else if($pvm[4] == '.') {
				$osa = substr($osa,0,2).(".0").(substr($pvm,3,strlen($pvm)));
				$pvm = $osa;
				
		}	return $pvm;
}
if($_SERVER["REQUEST_METHOD"] == "POST" && $_COOKIE["testi"] == "Asiakkaat"){
$etunimi=mysqli_real_escape_string($dbconfig,$_POST['Etu']);
$sukunimi=mysqli_real_escape_string($dbconfig,$_POST['Suku']);

$sql_query="SELECT ID FROM asiakas WHERE Etunimi='$etunimi' and Sukunimi='$sukunimi'";
$result=mysqli_query($dbconfig,$sql_query);
$count=mysqli_num_rows($result);
if($etunimi != null && $sukunimi !=null){
	if($count == 0){
		$etunimi=$_POST['Etu'];
		$sukunimi=$_POST['Suku'];
		$sql="INSERT INTO asiakas(Etunimi,Sukunimi) VALUES ('$etunimi','$sukunimi')";
		if(mysqli_query($dbconfig,$sql)){
			echo "<script>alert('Asiakkaan lisääminen tietokantaan onnistui!');</script>";
			mysqli_free_result($result);
		}else{
		echo "<script>alert('Asiakkaan lisääminen tietokantaan ei onnistunut!');</script>";
		}
	}else{
		echo "<script>alert('Asiakas on jo tietokannassa!');</script>";
	}
}else {
	echo "<script>alert('Tarkista syöttämäsi tiedot!');</script>";
}
 }
 else if($_SERVER["REQUEST_METHOD"] == "POST" && $_COOKIE["testi"] == "Celssin_Testi"){

$etunimi=mysqli_real_escape_string($dbconfig,$_POST['Etu']);
$sukunimi=mysqli_real_escape_string($dbconfig,$_POST['Suku']);
$sql_query="SELECT ID FROM asiakas WHERE Etunimi='$etunimi' and Sukunimi='$sukunimi'";
$result=mysqli_query($dbconfig,$sql_query);
$count=mysqli_num_rows($result);
$id = $_POST['ID'];
$pvm = $_POST['Pvm'];
$pvm = pvmtark($pvm);

$Aika22s = $_POST['Aika22s'];
$Aika25s = $_POST['Aika25s'];
$Aika29s = $_POST['Aika29s'];

if($id != null && $etunimi != null && $sukunimi !=null && $pvm != null){
	if($count != 0){
		$sql="INSERT INTO celssin_testi(ID,Etunimi,Sukunimi,Paivamaara, Aika22s, Aika25s, Aika29s) VALUES ('$id','$etunimi','$sukunimi','$pvm','$Aika22s','$Aika25s','$Aika29s')";
		if(mysqli_query($dbconfig,$sql)){
			echo "<script>alert('Tuloksien tallentaminen tietokantaan onnistui!');</script>";
		}else{
			echo "<script>alert('Tuloksien tallentaminen tietokantaan ei onnistunut!');</script>";
		}
	}else{
		echo "<script>alert('Tarkista syöttämäsi tiedot!');</script>";
	}
}else {
	echo "<script>alert('Tarkista syöttämäsi tiedot !');</script>";
}
 }
  else if($_SERVER["REQUEST_METHOD"] == "POST" && $_COOKIE["testi"] == "Hyppy_Testi"){
$etunimi=mysqli_real_escape_string($dbconfig,$_POST['Etu']);
$sukunimi=mysqli_real_escape_string($dbconfig,$_POST['Suku']);
$sql_query="SELECT ID FROM asiakas WHERE Etunimi='$etunimi' and Sukunimi='$sukunimi'";
$result=mysqli_query($dbconfig,$sql_query);
$count=mysqli_num_rows($result);
$id = $_POST['ID'];
$etunimi=$_POST['Etu'];
$sukunimi=$_POST['Suku'];
$pvm = $_POST['Pvm'];
$pvm = pvmtark($pvm);
$Vauhditon = $_POST['Vauhditon'];
$Viisloikka = $_POST['Viisloikka'];
if($id != null && $etunimi != null && $sukunimi !=null && $pvm != null){
	if($count != 0){
		$sql="INSERT INTO hyppy_testi(ID,Etunimi,Sukunimi,Paivamaara, Vauhditon, Viisloikka) VALUES ('$id','$etunimi','$sukunimi','$pvm','$Vauhditon','$Viisloikka')";
		if(mysqli_query($dbconfig,$sql)){
			echo "<script>alert('Tuloksien tallentaminen tietokantaan onnistui!');</script>";
		}else{
			echo "<script>alert('Tuloksien tallentaminen tietokantaan ei onnistunut!');</script>";
		}
	}
	else{
		echo "<script>alert('Tarkista syöttämäsi tiedot!');</script>";
	}
}
else {
	echo "<script>alert('Tarkista syöttämäsi tiedot!');</script>";
}


 }
  else if($_SERVER["REQUEST_METHOD"] == "POST" && $_COOKIE["testi"] == "Matto_Testi"){
$etunimi=mysqli_real_escape_string($dbconfig,$_POST['Etu']);
$sukunimi=mysqli_real_escape_string($dbconfig,$_POST['Suku']);
$sql_query="SELECT ID FROM asiakas WHERE Etunimi='$etunimi' and Sukunimi='$sukunimi'";
$result=mysqli_query($dbconfig,$sql_query);
$count=mysqli_num_rows($result);
$id = $_POST['ID'];
$etunimi=$_POST['Etu'];
$sukunimi=$_POST['Suku'];
$pvm = $_POST['Pvm'];
$pvm = pvmtark($pvm);
$SV = $_POST['SV'];
$SO = $_POST['SO'];
$EV = $_POST['EV'];
$EO = $_POST['EO'];
$SK = $_POST['SK'];
$EK = $_POST['EK'];
if($id != null && $etunimi != null && $sukunimi !=null && $pvm != null){
	if($count != 0){
	$sql="INSERT INTO matto_testi(ID,Etunimi,Sukunimi,Paivamaara, Stabiili_v, Stabiili_o,Esikevennetty_v,Esikevennetty_o,Stabiili_k,Esikevennetty_k) VALUES ('$id','$etunimi','$sukunimi','$pvm','$SV','$SO','$EV','$EO','$SK','$VK')";
		if(mysqli_query($dbconfig,$sql)){
			echo "<script>alert('Tuloksien tallentaminen tietokantaan onnistui!');</script>";
		}else{
			echo "<script>alert('Tuloksien tallentaminen tietokantaan ei onnistunut!');</script>";
		}
	}else{
		echo "<script>alert('Tarkista syöttämäsi tiedot!');</script>";
	}
	}
else {
	echo "<script>alert('Tarkista syöttämäsi tiedot!');</script>";
}


 }
  else if($_SERVER["REQUEST_METHOD"] == "POST" && $_COOKIE["testi"] == "ViisKymppiViis_Testi"){
$etunimi=mysqli_real_escape_string($dbconfig,$_POST['Etu']);
$sukunimi=mysqli_real_escape_string($dbconfig,$_POST['Suku']);
$sql_query="SELECT ID FROM asiakas WHERE Etunimi='$etunimi' and Sukunimi='$sukunimi'";
$result=mysqli_query($dbconfig,$sql_query);
$count=mysqli_num_rows($result);
$id = $_POST['ID'];
$etunimi=$_POST['Etu'];
$sukunimi=$_POST['Suku'];
$pvm = $_POST['Pvm'];
$pvm = pvmtark($pvm);
$Eka = $_POST['Eka'];
$Toka = $_POST['Toka'];
if($id != null && $etunimi != null && $sukunimi !=null && $pvm != null){
	if($count != 0){
	$sql="INSERT INTO viiskymppiviis_testi(ID,Etunimi,Sukunimi,Paivamaara, Eka,Toka) VALUES ('$id','$etunimi','$sukunimi','$pvm','$Eka','$Toka')";
		if(mysqli_query($dbconfig,$sql)){
			echo "<script>alert('Tuloksien tallentaminen tietokantaan onnistui!');</script>";
		}else{
			echo "<script>alert('Tuloksien tallentaminen tietokantaan ei onnistunut!');</script>";
		}
	}else{
		echo "<script>alert('Tarkista syöttämäsi tiedot!');</script>";
	}
	}
else {
	echo "<script>alert('Tarkista syöttämäsi tiedot!');</script>";
}


 }
  else if($_SERVER["REQUEST_METHOD"] == "POST" && $_COOKIE["testi"] == "Jaa_Testi"){
$etunimi=mysqli_real_escape_string($dbconfig,$_POST['Etu']);
$sukunimi=mysqli_real_escape_string($dbconfig,$_POST['Suku']);
$sql_query="SELECT ID FROM asiakas WHERE Etunimi='$etunimi' and Sukunimi='$sukunimi'";
$result=mysqli_query($dbconfig,$sql_query);
$count=mysqli_num_rows($result);
$id = $_POST['ID'];
$etunimi=$_POST['Etu'];
$sukunimi=$_POST['Suku'];
$pvm = $_POST['Pvm'];
$pvm = pvmtark($pvm);
$JN = $_POST['JN'];
$JC = $_POST['JC'];
if($id != null && $etunimi != null && $sukunimi !=null && $pvm != null){
	if($count != 0){
	$sql="INSERT INTO jaa_testi(ID,Etunimi,Sukunimi,Paivamaara, JaaNopeus,JaaCelssi) VALUES ('$id','$etunimi','$sukunimi','$pvm','$JN','$JC')";
		if(mysqli_query($dbconfig,$sql)){
			echo "<script>alert('Tuloksien tallentaminen tietokantaan onnistui!');</script>";
		}else{
			echo "<script>alert('Tuloksien tallentaminen tietokantaan ei onnistunut!');</script>";
		}
	}else{
		echo "<script>alert('Tarkista syöttämäsi tiedot!');</script>";
	}
	}
else {
	echo "<script>alert('Tarkista syöttämäsi tiedot!');</script>";
}

 }
 unset($_COOKIE["testi"]);
 $res = setcookie("testi",null,time()-3600);
 $image5_url="logo3_6.png";
 $image_url="3d.png";
 $image_url2="3d_2.png";
 $image_url3="3d_3.png";
 
?>

<!DOCTYPE html>
<html>
<head>
<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<meta charset="UTF-8" name="viewport" content="initial-scale=1.0",width=device-width"/>
<link rel="stylesheet" type="text/css" href="style.css">
<script type="text/javascript">
window.setTimeout("location=('logout.php');",600000);
</script>
<title>Testitulokset lisää</title>


</head>

<body>

<div class="wow"><img src="<?php echo $image_url3?>"></div>
<div class="login-block">
<form align="left" name="form1" method="post">

<button type="submit" class="button2" name="napit" id="etusivu"value="etusivu">Etusivu</button>
<button type="submit" class="button2" name="napit" id="haku"value="haku">Haku</button>
<button type="submit" class="button2" name="napit" id="logout"value="logout">Logout</button>

</form>

    <?php
	
	if(isset($_SESSION['login_user']))
	{
	$login_session=$_SESSION['login_user'];
	echo '<div class="Etu" align="center"><h1>Tervetuloa '.$login_session.'</h1></div>';
	echo '<h3>Lisää testituloksia tietokantaan : </h3>';
	}
	?>

<script src="Testit.js"></script>

<form class="formula">
<p>Valitse testi:</p>
<select name="testit" onchange="nayta2(this.value)">
<option value="Valitse testi">Valitse Testi</option>
<option value="Asiakkaat">Asiakkaat</option>
<option value="Celssin_Testi">Celssin testi</option>
<option value="Hyppy_Testi">Hyppy testi</option>
<option value="Matto_Testi">Matto testi</option>
<option value="ViisKymppiViis_Testi">Viis kymppi viis testi</option>
<option value="Jaa_Testi">Jää testi</option> 
</select>
</form>
<br>
<div class="tulokset"><p>Lisää tulokset :</p></div>
<br>
<div class="tulokset" id="txtHint2"><p></p></div>
	<br><br>
	
</div>
<div class="logo"><img src="<?php echo $image5_url?>"></div>
</body>

</html>
