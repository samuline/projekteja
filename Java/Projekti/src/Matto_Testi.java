
import javax.persistence.*;

@Entity
@Table(name = "matto_testi")
public class Matto_Testi {

    private int Numero, ID;
    private String Etunimi, Sukunimi, Paivamaara, Stabiili_v, Stabiili_o, Stabiili_k, Esikevennetty_v, Esikevennetty_o, Esikevennetty_k;

    public Matto_Testi(int Numero, int ID, String Etunimi, String Sukunimi, String Paivamaara, String Stabiili_v, String Stabiili_o, String Esikevennetty_v, String Esikevennetty_o, String Stabiili_k, String Esikevennetty_k) {
        this.Numero = Numero;
        this.ID = ID;
        this.Etunimi = Etunimi;
        this.Sukunimi = Sukunimi;
        this.Paivamaara = Paivamaara;
        this.Stabiili_v = Stabiili_v;
        this.Stabiili_o = Stabiili_o;
        this.Stabiili_k = Stabiili_k;
        this.Esikevennetty_v = Esikevennetty_v;
        this.Esikevennetty_o = Esikevennetty_o;
        this.Esikevennetty_k = Esikevennetty_k;
    }

    public Matto_Testi() {
    }

    @Id
    @GeneratedValue
    @Column(name = "Numero")
    public int getNumero() {
        return Numero;
    }

    public void setNumero(int Numero) {
        this.Numero = Numero;
    }

    @Column(name = "ID")
    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    @Column(name = "Etunimi")
    public String getEtunimi() {
        return Etunimi;
    }

    public void setEtunimi(String Etunimi) {
        this.Etunimi = Etunimi;
    }

    @Column(name = "Sukunimi")
    public String getSukunimi() {
        return Sukunimi;
    }

    public void setSukunimi(String Sukunimi) {
        this.Sukunimi = Sukunimi;
    }

    @Column(name = "Paivamaara")
    public String getPaivamaara() {
        return Paivamaara;
    }

    public void setPaivamaara(String Paivamaara) {
        this.Paivamaara = Paivamaara;
    }

    @Column(name = "Stabiili_v")
    public String getStabiili_v() {
        return Stabiili_v;
    }

    public void setStabiili_v(String Stabiili_v) {
        this.Stabiili_v = Stabiili_v;
    }

    @Column(name = "Stabiili_o")
    public String getStabiili_o() {
        return Stabiili_o;
    }

    public void setStabiili_o(String Stabiili_o) {
        this.Stabiili_o = Stabiili_o;
    }

    @Column(name = "Stabiili_k")
    public String getStabiili_k() {
        return Stabiili_k;
    }

    public void setStabiili_k(String Stabiili_k) {
        this.Stabiili_k = Stabiili_k;
    }

    @Column(name = "Esikevennetty_v")
    public String getEsikevennetty_v() {
        return Esikevennetty_v;
    }

    public void setEsikevennetty_v(String Esikevennetty_v) {
        this.Esikevennetty_v = Esikevennetty_v;
    }

    @Column(name = "Esikevennetty_o")
    public String getEsikevennetty_o() {
        return Esikevennetty_o;
    }

    public void setEsikevennetty_o(String Esikevennetty_o) {
        this.Esikevennetty_o = Esikevennetty_o;
    }

    @Column(name = "Esikevennetty_k")
    public String getEsikevennetty_k() {
        return Esikevennetty_k;
    }

    public void setEsikevennetty_k(String Esikevennetty_k) {
        this.Esikevennetty_k = Esikevennetty_k;
    }

    public String XML() {
        StringBuffer sb = new StringBuffer();
        sb.append("<Matto_Testi>\n");
        sb.append("\t<ID>\t" + ID + "\t</ID>\n");
        sb.append("\t<Etunimi>\t" + Etunimi + "\t</Etunimi>\n");
        sb.append("\t<Sukunimi>\t" + Sukunimi + "\t</Sukunimi>\n");
        sb.append("\t<Paivamaara>\t" + Paivamaara + "\t</Paivamaara>\n");
        sb.append("\t<Stabiili_v>\t" + Stabiili_v + "\t</Stabiili_v>\n");
        sb.append("\t<Stabiili_o>\t" + Stabiili_o + "\t</Stabiili_o>\n");
        sb.append("\t<Esikevennetty_v>\t" + Esikevennetty_v + "\t</Esikevennetty_v>\n");
        sb.append("\t<Esikevennetty_o>\t" + Esikevennetty_o + "\t</Esikevennetty_o>\n");
        sb.append("\t<Stabiili_k>\t" + Stabiili_k + "\t</Stabiili_k>\n");
        sb.append("\t<Esikevennetty_k>\t" + Esikevennetty_k + "\t</Esikevennetty_k>\n");
        sb.append("</Matto_Testi>\n");
        return sb.toString();
    }

}
