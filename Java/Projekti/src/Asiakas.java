
import javax.persistence.*;

@Entity
@Table(name = "asiakas")
public class Asiakas {

    private String Etunimi, Sukunimi;
    private int ID;

    public Asiakas(int ID, String Etunimi, String Sukunimi) {
        this.Etunimi = Etunimi;
        this.Sukunimi = Sukunimi;
        this.ID = ID;
    }

    public Asiakas() {
    }

    @Column(name = "Etunimi")
    public String getEtunimi() {
        return Etunimi;
    }

    public void setEtunimi(String Etunimi) {
        this.Etunimi = Etunimi;
    }

    @Column(name = "Sukunimi")
    public String getSukunimi() {
        return Sukunimi;
    }

    public void setSukunimi(String Sukunimi) {
        this.Sukunimi = Sukunimi;
    }

    @Id
    @GeneratedValue
    @Column(name = "ID")
    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

}
