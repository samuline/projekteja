
import javax.persistence.*;

@Entity
@Table(name = "viiskymppiviis_testi")
public class ViisKymppiViis_Testi {
     private int Numero,ID;
    private String Etunimi,Sukunimi,Paivamaara, Eka,Toka;

    public ViisKymppiViis_Testi(int Numero, int ID, String Etunimi, String Sukunimi, String Paivamaara, String Eka, String Toka) {
        this.Numero = Numero;
        this.ID = ID;
        this.Etunimi = Etunimi;
        this.Sukunimi = Sukunimi;
        this.Paivamaara = Paivamaara;
        this.Eka = Eka;
        this.Toka = Toka;
    }

    public ViisKymppiViis_Testi() {
    }
    @Id
    @GeneratedValue
    @Column(name = "Numero")
    public int getNumero() {
        return Numero;
    }

    public void setNumero(int Numero) {
        this.Numero = Numero;
    }
    @Column(name = "ID")
    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }
    @Column(name = "Etunimi")
    public String getEtunimi() {
        return Etunimi;
    }

    public void setEtunimi(String Etunimi) {
        this.Etunimi = Etunimi;
    }
    @Column(name = "Sukunimi")
    public String getSukunimi() {
        return Sukunimi;
    }

    public void setSukunimi(String Sukunimi) {
        this.Sukunimi = Sukunimi;
    }
    @Column(name = "Paivamaara")
    public String getPaivamaara() {
        return Paivamaara;
    }

    public void setPaivamaara(String Paivamaara) {
        this.Paivamaara = Paivamaara;
    }
    @Column(name = "Eka")
    public String getEka() {
        return Eka;
    }

    public void setEka(String Eka) {
        this.Eka = Eka;
    }
    @Column(name = "Toka")
    public String getToka() {
        return Toka;
    }

    public void setToka(String Toka) {
        this.Toka = Toka;
    }
    
    public String XML() {
        StringBuffer sb = new StringBuffer();
        sb.append("<ViisKymppiViis_Testi>\n");
        sb.append("\t<ID>\t" + ID + "\t</ID>\n");
        sb.append("\t<Etunimi>\t" + Etunimi + "\t</Etunimi>\n");
        sb.append("\t<Sukunimi>\t" + Sukunimi + "\t</Sukunimi>\n");
        sb.append("\t<Paivamaara>\t" + Paivamaara + "\t</Paivamaara>\n");
        sb.append("\t<Eka>\t" + Eka + "\t</Eka>\n");
        sb.append("\t<Toka>\t" + Toka + "\t</Toka>\n");       
        sb.append("</ViisKymppiViis_Testi>\n");
        return sb.toString();
    }
    
}
