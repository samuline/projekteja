<?xml version="1.0" encoding="UTF-8"?><fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
<fo:layout-master-set>
<fo:simple-page-master master-name="simpleA4" page-height="29.7cm" page-width="21cm" margin-top="2cm" margin-bottom="2cm" margin-left="2cm" margin-right="2cm">
<fo:region-before region-name="region-before" extent="11in"/>
<fo:region-body/>
</fo:simple-page-master>
</fo:layout-master-set>
<fo:page-sequence master-reference="simpleA4">
<fo:static-content flow-name="region-before">
<fo:block>Testitulokset :</fo:block>
</fo:static-content>
<fo:flow flow-name="xsl-region-body">
<fo:block font-size="16pt" font-weight="bold" space-before="5mm" space-after="5mm">testi: </fo:block>
<fo:block font-size="10pt">
<fo:table table-layout="fixed">
<fo:table-column column-width="3cm"/>
<fo:table-column column-width="3cm"/>
<fo:table-column column-width="3cm"/>
<fo:table-column column-width="3cm"/>
<fo:table-column column-width="3cm"/>
<fo:table-body>
<fo:table-row>
<fo:table-cell>
<fo:block>	2	</fo:block>
</fo:table-cell>
<fo:table-cell>
<fo:block>	Seppo	</fo:block>
</fo:table-cell>
<fo:table-cell>
<fo:block>	MeikÃ¤lÃ¤inen	</fo:block>
</fo:table-cell>
<fo:table-cell>
<fo:block>	12.04.2016	</fo:block>
</fo:table-cell>
<fo:table-cell>
<fo:block>	100sec	</fo:block>
</fo:table-cell>
<fo:table-cell>
<fo:block>	120sec	</fo:block>
</fo:table-cell>
<fo:table-cell>
<fo:block>	130sec	</fo:block>
</fo:table-cell>
</fo:table-row>

<fo:table-row>
<fo:table-cell>
<fo:block>	3	</fo:block>
</fo:table-cell>
<fo:table-cell>
<fo:block>	Veikko	</fo:block>
</fo:table-cell>
<fo:table-cell>
<fo:block>	Nutturainen	</fo:block>
</fo:table-cell>
<fo:table-cell>
<fo:block>	12.04.2016	</fo:block>
</fo:table-cell>
<fo:table-cell>
<fo:block>	100sec	</fo:block>
</fo:table-cell>
<fo:table-cell>
<fo:block>	120sec	</fo:block>
</fo:table-cell>
<fo:table-cell>
<fo:block>	130sec	</fo:block>
</fo:table-cell>
</fo:table-row>

<fo:table-row>
<fo:table-cell>
<fo:block>	1	</fo:block>
</fo:table-cell>
<fo:table-cell>
<fo:block>	Matti	</fo:block>
</fo:table-cell>
<fo:table-cell>
<fo:block>	MeikÃ¤lÃ¤inen	</fo:block>
</fo:table-cell>
<fo:table-cell>
<fo:block>	19.04.2016	</fo:block>
</fo:table-cell>
<fo:table-cell>
<fo:block>	140sec	</fo:block>
</fo:table-cell>
<fo:table-cell>
<fo:block>	140sec	</fo:block>
</fo:table-cell>
<fo:table-cell>
<fo:block>	140sec	</fo:block>
</fo:table-cell>
</fo:table-row>

<fo:table-row>
<fo:table-cell>
<fo:block>	3	</fo:block>
</fo:table-cell>
<fo:table-cell>
<fo:block>	Veikko	</fo:block>
</fo:table-cell>
<fo:table-cell>
<fo:block>	Nutturainen	</fo:block>
</fo:table-cell>
<fo:table-cell>
<fo:block>	24.02.2016	</fo:block>
</fo:table-cell>
<fo:table-cell>
<fo:block>	123s	</fo:block>
</fo:table-cell>
<fo:table-cell>
<fo:block>	125s	</fo:block>
</fo:table-cell>
<fo:table-cell>
<fo:block>	127s	</fo:block>
</fo:table-cell>
</fo:table-row>

</fo:table-body>
</fo:table>
</fo:block>
</fo:flow>
</fo:page-sequence>
</fo:root>
