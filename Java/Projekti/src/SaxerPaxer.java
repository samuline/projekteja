
import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.swing.JOptionPane;
import javax.xml.parsers.*;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import org.xml.sax.*;
import org.xml.sax.helpers.*;

public class SaxerPaxer
        extends DefaultHandler {

    PrintWriter outF;
    private String st;
    private List<Celssin_Testi> celssil = null;
    private List<Hyppy_Testi> hyppyl = null;
    private List<Matto_Testi> mattol = null;
    private List<Jaa_Testi> jaal = null;
    private List<ViisKymppiViis_Testi> viisl = null;
    private Celssin_Testi celssiT = null;
    private Hyppy_Testi hyppyT = null;
    private Matto_Testi mattoT = null;
    private Jaa_Testi jaaT = null;
    private ViisKymppiViis_Testi viisT = null;
    private int testi = 0;
    private final File outFile = new File("kaikki.xml");

    private List testiC;
    private List testiM;
    private List testiH;
    private List testiV;
    private List testiJ;

    public static void main(String[] args) {
        SaxerPaxer s = new SaxerPaxer();
        s.sax();

        //s.vieDB();
        
        //s.tarkistaC();
    }

    public void tulosta() {
        for (int i = 0; i < celssil.size(); i++) {
            System.out.println(celssil.size());
            Celssin_Testi c = getCelssin1(i);
            System.out.println("\nCelssin Testi");
            System.out.println(c.getID());
            System.out.println(c.getEtunimi());
            System.out.println(c.getSukunimi());
            System.out.println(c.getPaivamaara());
            System.out.println(c.getAika22s());
            System.out.println(c.getAika25s());
            System.out.println(c.getAika29s());
        }
        for (int i = 0; i < mattol.size(); i++) {
            Matto_Testi m = getMatto1(i);
            System.out.println("\nMatto testi");
            System.out.println(m.getID());
            System.out.println(m.getEtunimi());
            System.out.println(m.getSukunimi());
            System.out.println(m.getPaivamaara());
            System.out.println(m.getStabiili_v());
            System.out.println(m.getStabiili_o());
            System.out.println(m.getEsikevennetty_v());
            System.out.println(m.getEsikevennetty_o());
            System.out.println(m.getStabiili_k());
            System.out.println(m.getEsikevennetty_k());
        }
        for (int i = 0; i < hyppyl.size(); i++) {
            Hyppy_Testi h = getHyppy1(i);
            System.out.println("\nHyppy testi");
            System.out.println(h.getID());
            System.out.println(h.getEtunimi());
            System.out.println(h.getSukunimi());
            System.out.println(h.getPaivamaara());
            System.out.println(h.getVauhditon());
            System.out.println(h.getViisloikka());

        }
        for (int i = 0; i < viisl.size(); i++) {
            ViisKymppiViis_Testi v = getViis1(i);
            System.out.println("\nViis Testi");
            System.out.println(v.getID());
            System.out.println(v.getEtunimi());
            System.out.println(v.getSukunimi());
            System.out.println(v.getPaivamaara());
            System.out.println(v.getEka());
            System.out.println(v.getToka());

        }
        for (int i = 0; i < jaal.size(); i++) {
            Jaa_Testi j = getJaa1(i);
            System.out.println("\nJää testi");
            System.out.println(j.getID());
            System.out.println(j.getEtunimi());
            System.out.println(j.getSukunimi());
            System.out.println(j.getPaivamaara());
            System.out.println(j.getJaaNopeus());
            System.out.println(j.getJaaCelssi());

        }
    }

    public void sax() {

        String uri = "src\\kaikki.xml";
        try {
            SAXParserFactory parserFactory = SAXParserFactory.newInstance();
            parserFactory.setValidating(true);
            parserFactory.setNamespaceAware(false);
            parserFactory.setFeature(
                    "http://apache.org/xml/features/validation/schema", true);
            SaxerPaxer MySaxParserInstance = new SaxerPaxer();
            SAXParser parser = parserFactory.newSAXParser();
            parser.parse(uri, MySaxParserInstance);
        } catch (IOException | ParserConfigurationException | FactoryConfigurationError exception) {
        } catch (SAXException exception) {
            System.out.println(exception.getMessage());
        }

    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {

        String s = new String(ch, start, length);
        try {
            
            if (testi == 0) {
                switch (st) {
                    case "ID":
                        celssiT.setID(Integer.parseInt(s.trim()));
                        break;
                    case "Etunimi":
                        celssiT.setEtunimi(s.trim());
                        break;
                    case "Sukunimi":
                        celssiT.setSukunimi(s.trim());
                        break;
                    case "Paivamaara":
                        celssiT.setPaivamaara(s.trim());
                        break;
                    case "Aika22s":
                        celssiT.setAika22s(s.trim());
                        break;
                    case "Aika25s":
                        celssiT.setAika25s(s.trim());
                        break;
                    case "Aika29s":
                        celssiT.setAika29s(s.trim());
                        break;
                    case "elementti":
                        System.out.println("nulli? " + st + " ässä " + s);
                        break;
                }
            } else if (testi == 1) {
                switch (st) {
                    case "ID":
                        hyppyT.setID(Integer.parseInt(s.trim()));
                        break;
                    case "Etunimi":
                        hyppyT.setEtunimi(s.trim());
                        break;
                    case "Sukunimi":
                        hyppyT.setSukunimi(s.trim());
                        break;
                    case "Paivamaara":
                        hyppyT.setPaivamaara(s.trim());
                        break;
                    case "Vauhditon":
                        hyppyT.setVauhditon(s.trim());
                        break;
                    case "Viisloikka":
                        hyppyT.setViisloikka(s.trim());
                        break;
                    case "elementti":
                        System.out.println("nulli? " + st + " ässä " + s);
                        break;
                }

            } else if (testi == 2) {
                switch (st) {
                    case "ID":
                        mattoT.setID(Integer.parseInt(s.trim()));
                        break;
                    case "Etunimi":
                        mattoT.setEtunimi(s.trim());
                        break;
                    case "Sukunimi":
                        mattoT.setSukunimi(s.trim());
                        break;
                    case "Paivamaara":
                        mattoT.setPaivamaara(s.trim());
                        break;
                    case "Stabiili_v":
                        mattoT.setStabiili_v(s.trim());
                        System.out.println(mattoT.getStabiili_v());
                        break;
                    case "Stabiili_o":
                        mattoT.setStabiili_o(s.trim());
                        break;
                    case "Esikevennetty_v":
                        mattoT.setEsikevennetty_v(s.trim());
                        break;
                    case "Esikevennetty_o":
                        mattoT.setEsikevennetty_o(s.trim());
                        break;
                    case "Stabiili_k":
                        mattoT.setStabiili_k(s.trim());
                        break;
                    case "Esikevennetty_k":
                        mattoT.setEsikevennetty_k(s.trim());
                        break;
                    case "elementti":
                        System.out.println("nulli? " + st + " ässä " + s);
                        break;
                }
            } else if (testi == 3) {
                switch (st) {
                    case "ID":
                        viisT.setID(Integer.parseInt(s.trim()));
                        break;
                    case "Etunimi":

                        viisT.setEtunimi(s.trim());
                        break;
                    case "Sukunimi":
                        viisT.setSukunimi(s.trim());
                        break;
                    case "Paivamaara":
                        viisT.setPaivamaara(s.trim());
                        break;
                    case "Eka":
                        viisT.setEka(s.trim());
                        break;
                    case "Toka":
                        viisT.setToka(s.trim());
                        break;
                    case "elementti":
                        System.out.println("nulli? " + st + " ässä " + s);
                        break;
                }

            } else if (testi == 4) {
                switch (st) {
                    case "ID":
                        jaaT.setID(Integer.parseInt(s.trim()));
                        break;
                    case "Etunimi":
                        jaaT.setEtunimi(s.trim());
                        break;
                    case "Sukunimi":
                        jaaT.setSukunimi(s.trim());
                        break;
                    case "Paivamaara":
                        jaaT.setPaivamaara(s.trim());
                        break;
                    case "JaaNopeus":
                        jaaT.setJaaNopeus(s.trim());
                        break;
                    case "JaaCelssi":
                        jaaT.setJaaCelssi(s.trim());
                        break;
                    case "elementti":
                        System.out.println("nulli? " + st + " ässä " + s);
                        break;
                }

            }
        } catch (Exception e) {
            System.out.println("Number?");
        }
    }

    @Override
    public void endDocument() throws SAXException {
        System.out.println("Doka lopu");
        vieDB();
        tulosta();

    }

    @Override
    public void endElement(String url, String localName, String qName) throws
            SAXException {
        System.out.println("elementti loppuu " + qName);
        switch (qName) {
            case "Celssin_Testi":
                celssil.add(celssiT);

                break;
            case "Hyppy_Testi":
                hyppyl.add(hyppyT);
                break;
            case "Matto_Testi":
                mattol.add(mattoT);
                break;
            case "Jaa_Testi":
                jaal.add(jaaT);
                break;
            case "ViisKymppiViis_Testi":
                viisl.add(viisT);
                break;
            default:
                st = "elementti";
                break;
        }

    }

    @Override
    public void startDocument() throws SAXException {
        celssil = new ArrayList<>();
        hyppyl = new ArrayList<>();
        mattol = new ArrayList<>();
        jaal = new ArrayList<>();
        viisl = new ArrayList<>();
        testi = 0;
    }

    @Override
    public void startElement(String uri, String localName, String qName,
            Attributes attributes) throws SAXException {

        System.out.println("Elementti alkaa: " + qName);

        switch (qName) {
            case "Celssin_Testi":
                celssiT = new Celssin_Testi();
                break;
            case "Hyppy_Testi":
                hyppyT = new Hyppy_Testi();
                testi = 1;
                break;
            case "Matto_Testi":
                mattoT = new Matto_Testi();
                testi = 2;
                break;
            case "Jaa_Testi":
                jaaT = new Jaa_Testi();
                testi = 4;
                break;
            case "ViisKymppiViis_Testi":
                viisT = new ViisKymppiViis_Testi();
                testi = 3;
                break;
            case "ID":
                st = qName;
                break;
            default:
                st = qName;
                break;
        }

    }

    @Override
    public void fatalError(SAXParseException e) throws SAXException {
        throw new java.lang.UnsupportedOperationException(
                "Method fatalError() not yet implemented.");
    }

    public void openFile() throws IOException {

        outF = new PrintWriter(new FileOutputStream(outFile), true);

    }

    public void write(String row) {

        if (outF == null) {
            JOptionPane.showMessageDialog(null, "Ongelma tiedoston avauksessa!");
        } else {
            outF.println(row);
        }
    }

    public Celssin_Testi getCelssin1(int i) {

        return (Celssin_Testi) celssil.get(i);
    }

    public Matto_Testi getMatto1(int i) {
        return (Matto_Testi) mattol.get(i);
    }

    public Hyppy_Testi getHyppy1(int i) {
        return (Hyppy_Testi) hyppyl.get(i);
    }

    public ViisKymppiViis_Testi getViis1(int i) {
        return (ViisKymppiViis_Testi) viisl.get(i);
    }

    public Jaa_Testi getJaa1(int i) {
        return (Jaa_Testi) jaal.get(i);
    }

    public void vieDB() {
        dBConnection dB = dBConnection.getInstance();
        Session istunto = dBConnection.getIstuntotehdas().openSession();
        Transaction transaktio = null;
        testiC = new ArrayList();
        testiM = new ArrayList();
        testiH = new ArrayList();
        testiV = new ArrayList();
        testiJ = new ArrayList();
        List lista = null;
        //int i = 0;
        try {
            transaktio = istunto.beginTransaction();
            lista = istunto.createQuery("FROM Celssin_Testi").list();
            for (Iterator it = lista.iterator(); it.hasNext();) {
                Celssin_Testi celssi = (Celssin_Testi) it.next();
                Celssin_Testi lisääC = new Celssin_Testi();
                lisääC.setID(celssi.getID());
                lisääC.setEtunimi(celssi.getEtunimi());
                lisääC.setSukunimi(celssi.getSukunimi());
                lisääC.setPaivamaara(celssi.getPaivamaara());
                lisääC.setAika22s(celssi.getAika22s());
                lisääC.setAika25s(celssi.getAika25s());
                lisääC.setAika29s(celssi.getAika29s());
                
                testiC.add(lisääC);
                

            }
            lista = istunto.createQuery("FROM Matto_Testi").list();
            for (Iterator it = lista.iterator(); it.hasNext();) {
                Matto_Testi matto = (Matto_Testi) it.next();
                Matto_Testi lisääM = new Matto_Testi();
                lisääM.setID(matto.getID());
                lisääM.setEtunimi(matto.getEtunimi());
                lisääM.setSukunimi(matto.getSukunimi());
                lisääM.setPaivamaara(matto.getPaivamaara());
                lisääM.setStabiili_v(matto.getStabiili_v());
                lisääM.setStabiili_o(matto.getStabiili_o());
                lisääM.setEsikevennetty_v(matto.getEsikevennetty_v());
                lisääM.setEsikevennetty_o(matto.getEsikevennetty_o());
                lisääM.setStabiili_k(matto.getStabiili_k());
                lisääM.setEsikevennetty_k(matto.getEsikevennetty_k());

                testiM.add(lisääM);
                

            }
            lista = istunto.createQuery("FROM Hyppy_Testi").list();
            for (Iterator it = lista.iterator(); it.hasNext();) {
                Hyppy_Testi hyppy = (Hyppy_Testi) it.next();
                Hyppy_Testi lisääH = new Hyppy_Testi();
                lisääH.setID(hyppy.getID());
                lisääH.setEtunimi(hyppy.getEtunimi());
                lisääH.setSukunimi(hyppy.getSukunimi());
                lisääH.setPaivamaara(hyppy.getPaivamaara());
                lisääH.setVauhditon(hyppy.getVauhditon());
                lisääH.setViisloikka(hyppy.getViisloikka());

                testiH.add(lisääH);
                
            }
            lista = istunto.createQuery("FROM ViisKymppiViis_Testi ").list();
            for (Iterator it = lista.iterator(); it.hasNext();) {
                ViisKymppiViis_Testi viis = (ViisKymppiViis_Testi) it.next();
                ViisKymppiViis_Testi lisääV = new ViisKymppiViis_Testi();
                lisääV.setID(viis.getID());
                lisääV.setEtunimi(viis.getEtunimi());
                lisääV.setSukunimi(viis.getSukunimi());
                lisääV.setPaivamaara(viis.getPaivamaara());
                lisääV.setEka(viis.getEka());
                lisääV.setToka(viis.getToka());

                testiV.add(lisääV);
                

            }
            transaktio.commit();
            lista = istunto.createQuery("FROM Jaa_Testi ").list();
            for (Iterator it = lista.iterator(); it.hasNext();) {
                Jaa_Testi jaa = (Jaa_Testi) it.next();
                Jaa_Testi lisääJ = new Jaa_Testi();
                lisääJ.setID(jaa.getID());
                lisääJ.setEtunimi(jaa.getEtunimi());
                lisääJ.setSukunimi(jaa.getSukunimi());
                lisääJ.setPaivamaara(jaa.getPaivamaara());
                lisääJ.setJaaNopeus(jaa.getJaaNopeus());
                lisääJ.setJaaCelssi(jaa.getJaaCelssi());

                testiJ.add(lisääJ);
                
            }
            transaktio.commit();

        } catch (Exception e) {
            if (transaktio != null & transaktio.isActive()) {
                try {
                    transaktio.rollback();
                } catch (HibernateException e1) {

                }
            }
        } finally {
            istunto.close();
        }
    }

}
