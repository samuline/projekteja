
import javax.persistence.*;


@Entity
@Table(name = "kayttjat_t")
public class Kayttjat_t {
    
    private int numero;
    private String Tunnus, Salasana;

    public Kayttjat_t(int numero, String Tunnus, String Salasana) {
        this.numero = numero;
        this.Tunnus = Tunnus;
        this.Salasana = Salasana;
    }

    public Kayttjat_t() {
    }
    @Id
    @GeneratedValue
    @Column(name = "Numero")
    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }
    @Column(name = "Tunnus")
    public String getTunnus() {
        return Tunnus;
    }

    public void setTunnus(String Tunnus) {
        this.Tunnus = Tunnus;
    }
    @Column(name = "Salasana")
    public String getSalasana() {
        return Salasana;
    }

    public void setSalasana(String Salasana) {
        this.Salasana = Salasana;
    }
    
    
    
    
}
