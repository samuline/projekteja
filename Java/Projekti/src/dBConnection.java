
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;


public class dBConnection {
    private static dBConnection instance = null;
    private static SessionFactory istuntotehdas;
    private static StandardServiceRegistry builder;

    private dBConnection() {
        Configuration config = new Configuration().configure();
        builder = new StandardServiceRegistryBuilder().applySettings(config.getProperties()).build();
        istuntotehdas = config.buildSessionFactory(builder);
    }
   
   public static dBConnection getInstance(){
       if(instance == null){
           instance = new dBConnection();
       }
       return instance;
       
   }
   
   public static SessionFactory getIstuntotehdas(){
       return istuntotehdas;
   }
   
}
