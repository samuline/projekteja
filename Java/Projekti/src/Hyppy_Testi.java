
import javax.persistence.*;

@Entity
@Table(name = "hyppy_testi")
public class Hyppy_Testi {

    private int Numero, ID;
    private String Etunimi, Sukunimi, Paivamaara, Vauhditon, Viisloikka;

    public Hyppy_Testi(int Numero, int ID, String Etunimi, String Sukunimi, String Paivamaara, String Vauhditon, String Viisloikka) {
        this.Numero = Numero;
        this.ID = ID;
        this.Etunimi = Etunimi;
        this.Sukunimi = Sukunimi;
        this.Paivamaara = Paivamaara;
        this.Vauhditon = Vauhditon;
        this.Viisloikka = Viisloikka;
    }

    public Hyppy_Testi() {
    }

    @Id
    @GeneratedValue
    @Column(name = "Numero")
    public int getNumero() {
        return Numero;
    }

    public void setNumero(int Numero) {
        this.Numero = Numero;
    }

    @Column(name = "ID")
    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    @Column(name = "Etunimi")
    public String getEtunimi() {
        return Etunimi;
    }

    public void setEtunimi(String Etunimi) {
        this.Etunimi = Etunimi;
    }

    @Column(name = "Sukunimi")
    public String getSukunimi() {
        return Sukunimi;
    }

    public void setSukunimi(String Sukunimi) {
        this.Sukunimi = Sukunimi;
    }

    @Column(name = "Paivamaara")
    public String getPaivamaara() {
        return Paivamaara;
    }

    public void setPaivamaara(String Paivamaara) {
        this.Paivamaara = Paivamaara;
    }

    @Column(name = "Vauhditon")
    public String getVauhditon() {
        return Vauhditon;
    }

    public void setVauhditon(String Vauhditon) {
        this.Vauhditon = Vauhditon;
    }

    @Column(name = "Viisloikka")
    public String getViisloikka() {
        return Viisloikka;
    }

    public void setViisloikka(String Viisloikka) {
        this.Viisloikka = Viisloikka;
    }

    public String XML() {
        StringBuffer sb = new StringBuffer();
        sb.append("<Hyppy_Testi>\n");
        sb.append("\t<ID>\t" + ID + "\t</ID>\n");
        sb.append("\t<Etunimi>\t" + Etunimi + "\t</Etunimi>\n");
        sb.append("\t<Sukunimi>\t" + Sukunimi + "\t</Sukunimi>\n");
        sb.append("\t<Paivamaara>\t" + Paivamaara + "\t</Paivamaara>\n");
        sb.append("\t<Vauhditon>\t" + Vauhditon + "\t</Vauhditon>\n");
        sb.append("\t<Viisloikka>\t" + Viisloikka + "\t</Viisloikka>\n");
        sb.append("</Hyppy_Testi>\n");
        return sb.toString();
    }

}
