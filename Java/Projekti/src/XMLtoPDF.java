
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import javax.swing.JOptionPane;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerException;
import javax.xml.transform.Source;
import javax.xml.transform.Result;
import javax.xml.transform.stream.StreamSource;
import javax.xml.transform.sax.SAXResult;

import org.apache.avalon.framework.logger.ConsoleLogger;
import org.apache.avalon.framework.logger.Logger;
import org.apache.avalon.framework.ExceptionUtil;

import org.apache.fop.apps.Driver;
import org.apache.fop.apps.FOPException;
import org.apache.fop.messaging.MessageHandler;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class XMLtoPDF extends Projekti_Menu {

    private List testiC = null;
    private List testiM = null;
    private List testiH = null;
    private List testiV = null;
    private List testiJ = null;
    private File baseDir;
    private File outDir;
    private File outFile;
    private File xml2;
    private File xsl;
    private File pdf2;
    PrintWriter outF;
    private String suku;
    private boolean nakki = false;

    public boolean XmlK(String paivamaara) {

        try {
            baseDir = new File(".");
            outDir = new File(baseDir, "Out");
            outFile = new File("testiXML.xml");
            xml2 = new File("testiXML.xml");
            xsl = new File("style.xsl");
            if (!suku.isEmpty()) {
                pdf2 = new File(outDir, suku + "_" + paivamaara + ".pdf");
                
                nakki = true;
            } 
            
            else {
                
                nakki = false;
            }
            outDir.mkdirs();
        } catch (Exception e) {

        }
        return nakki;
    }

    public void haeDB(int id, String pvm) {

        Session istunto = dBConnection.getIstuntotehdas().openSession();
        Transaction transaktio = null;
        testiC = new ArrayList();
        testiM = new ArrayList();
        testiH = new ArrayList();
        testiV = new ArrayList();
        testiJ = new ArrayList();
        List lista = null;
        try {
            transaktio = istunto.beginTransaction();
            lista = istunto.createQuery("FROM celssin_testi WHERE ID = " + id + "AND Paivamaara = " + "'" + pvm + "'").list();
            for (Iterator it = lista.iterator(); it.hasNext();) {
                Celssin_Testi celssi = (Celssin_Testi) it.next();
                Celssin_Testi lisääC = new Celssin_Testi();
                lisääC.setID(celssi.getID());
                lisääC.setEtunimi(celssi.getEtunimi());
                lisääC.setSukunimi(celssi.getSukunimi());
                if (!celssi.getSukunimi().isEmpty()) {
                    suku = celssi.getSukunimi();
                }
                lisääC.setPaivamaara(celssi.getPaivamaara());
                lisääC.setAika22s(celssi.getAika22s());
                lisääC.setAika25s(celssi.getAika25s());
                lisääC.setAika29s(celssi.getAika29s());

                testiC.add(lisääC);
            }
            lista = istunto.createQuery("FROM matto_testi WHERE ID = " + id + "AND Paivamaara = " + "'" + pvm + "'").list();
            for (Iterator it = lista.iterator(); it.hasNext();) {
                Matto_Testi matto = (Matto_Testi) it.next();
                Matto_Testi lisääM = new Matto_Testi();
                lisääM.setID(matto.getID());
                lisääM.setEtunimi(matto.getEtunimi());
                lisääM.setSukunimi(matto.getSukunimi());
                if (!matto.getSukunimi().isEmpty()) {
                    suku = matto.getSukunimi();
                }
                lisääM.setPaivamaara(matto.getPaivamaara());
                lisääM.setStabiili_v(matto.getStabiili_v());
                lisääM.setStabiili_o(matto.getStabiili_o());
                lisääM.setEsikevennetty_v(matto.getEsikevennetty_v());
                lisääM.setEsikevennetty_o(matto.getEsikevennetty_o());
                lisääM.setStabiili_k(matto.getStabiili_k());
                lisääM.setEsikevennetty_k(matto.getEsikevennetty_k());

                testiM.add(lisääM);

            }
            lista = istunto.createQuery("FROM hyppy_testi WHERE ID = " + id + "AND Paivamaara = " + "'" + pvm + "'").list();
            for (Iterator it = lista.iterator(); it.hasNext();) {
                Hyppy_Testi hyppy = (Hyppy_Testi) it.next();
                Hyppy_Testi lisääH = new Hyppy_Testi();
                lisääH.setID(hyppy.getID());
                lisääH.setEtunimi(hyppy.getEtunimi());
                lisääH.setSukunimi(hyppy.getSukunimi());
                if (!hyppy.getSukunimi().isEmpty()) {
                    suku = hyppy.getSukunimi();
                }
                lisääH.setPaivamaara(hyppy.getPaivamaara());
                lisääH.setVauhditon(hyppy.getVauhditon());
                lisääH.setViisloikka(hyppy.getViisloikka());

                testiH.add(lisääH);

            }
            lista = istunto.createQuery("FROM viiskymppiviis_testi WHERE ID = " + id + "AND Paivamaara = " + "'" + pvm + "'").list();
            for (Iterator it = lista.iterator(); it.hasNext();) {
                ViisKymppiViis_Testi viis = (ViisKymppiViis_Testi) it.next();
                ViisKymppiViis_Testi lisääV = new ViisKymppiViis_Testi();
                lisääV.setID(viis.getID());
                lisääV.setEtunimi(viis.getEtunimi());
                lisääV.setSukunimi(viis.getSukunimi());
                if (!viis.getSukunimi().isEmpty()) {
                    suku = viis.getSukunimi();
                }
                lisääV.setPaivamaara(viis.getPaivamaara());
                lisääV.setEka(viis.getEka());
                lisääV.setToka(viis.getToka());

                testiV.add(lisääV);

            }
            transaktio.commit();
            lista = istunto.createQuery("FROM jaa_testi WHERE ID = " + id + "AND  Paivamaara = " + "'" + pvm + "'").list();
            for (Iterator it = lista.iterator(); it.hasNext();) {
                Jaa_Testi jaa = (Jaa_Testi) it.next();
                Jaa_Testi lisääJ = new Jaa_Testi();
                lisääJ.setID(jaa.getID());
                lisääJ.setEtunimi(jaa.getEtunimi());
                lisääJ.setSukunimi(jaa.getSukunimi());
                if (!jaa.getSukunimi().isEmpty()) {
                    suku = jaa.getSukunimi();
                }
                lisääJ.setPaivamaara(jaa.getPaivamaara());
                lisääJ.setJaaNopeus(jaa.getJaaNopeus());
                lisääJ.setJaaCelssi(jaa.getJaaCelssi());

                testiJ.add(lisääJ);

            }
            transaktio.commit();

        } catch (Exception e) {
            if (transaktio != null & transaktio.isActive()) {
                try {
                    transaktio.rollback();
                } catch (HibernateException e1) {

                }
            }
        } finally {
            istunto.close();
        }

    }

    public Celssin_Testi getCelssin(int i) {
        return (Celssin_Testi) testiC.get(i);
    }

    public Matto_Testi getMatto(int i) {
        return (Matto_Testi) testiM.get(i);
    }

    public Hyppy_Testi getHyppy(int i) {
        return (Hyppy_Testi) testiH.get(i);
    }

    public ViisKymppiViis_Testi getViis(int i) {
        return (ViisKymppiViis_Testi) testiV.get(i);
    }

    public Jaa_Testi getJaa(int i) {
        return (Jaa_Testi) testiJ.get(i);
    }

    public String toXML() {
        StringBuffer sb = new StringBuffer();
        sb.append(("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n"));
        //sb.append("<!DOCTYPE testit SYSTEM \"testit.dtd\">\n\n");
        sb.append("<testit>\n");
        sb.append("<Celssin_Testi>\n");
        sb.append("\t<ID>\t" + "ID" + "\t</ID>\n");
        sb.append("\t<Etunimi>\t" + "Etunimi" + "\t</Etunimi>\n");
        sb.append("\t<Sukunimi>\t" + "Sukunimi" + "\t</Sukunimi>\n");
        sb.append("\t<Paivamaara>\t" + "Päivämäärä" + "\t</Paivamaara>\n");
        sb.append("\t<Aika22s>\t" + "Aika22s" + "\t</Aika22s>\n");
        sb.append("\t<Aika25s>\t" + "Aika25s" + "\t</Aika25s>\n");
        sb.append("\t<Aika29s>\t" + "Aika29s" + "\t</Aika29s>\n");
        sb.append("</Celssin_Testi>\n");
        sb.append("<Celssin_Testi>\n");
        sb.append("\t<ID>\t" + "---" + "\t</ID>\n");
        sb.append("\t<Etunimi>\t" + "----------" + "\t</Etunimi>\n");
        sb.append("\t<Sukunimi>\t" + "-------------" + "\t</Sukunimi>\n");
        sb.append("\t<Paivamaara>\t" + "----------------" + "\t</Paivamaara>\n");
        sb.append("\t<Aika22s>\t" + "-----------" + "\t</Aika22s>\n");
        sb.append("\t<Aika25s>\t" + "-----------" + "\t</Aika25s>\n");
        sb.append("\t<Aika29s>\t" + "-----------" + "\t</Aika29s>\n");
        sb.append("</Celssin_Testi>\n");
        for (int i = 0; i < testiC.size(); i++) {
            sb.append(getCelssin(i).XML());
            sb.append("\n");
        }
        sb.append("<Hyppy_Testi>\n");
        sb.append("\t<ID>\t" + "ID" + "\t</ID>\n");
        sb.append("\t<Etunimi>\t" + "Etunimi" + "\t</Etunimi>\n");
        sb.append("\t<Sukunimi>\t" + "Sukunimi" + "\t</Sukunimi>\n");
        sb.append("\t<Paivamaara>\t" + "Päivämäärä" + "\t</Paivamaara>\n");
        sb.append("\t<Vauhditon>\t" + "Vauhditon" + "\t</Vauhditon>\n");
        sb.append("\t<Viisloikka>\t" + "Viisloikka" + "\t</Viisloikka>\n");
        sb.append("</Hyppy_Testi>\n");
        sb.append("<Hyppy_Testi>\n");
        sb.append("\t<ID>\t" + "---" + "\t</ID>\n");
        sb.append("\t<Etunimi>\t" + "----------" + "\t</Etunimi>\n");
        sb.append("\t<Sukunimi>\t" + "-------------" + "\t</Sukunimi>\n");
        sb.append("\t<Paivamaara>\t" + "----------------" + "\t</Paivamaara>\n");
        sb.append("\t<Vauhditon>\t" + "--------------" + "\t</Vauhditon>\n");
        sb.append("\t<Viisloikka>\t" + "-------------" + "\t</Viisloikka>\n");
        sb.append("</Hyppy_Testi>\n");
        for (int i = 0; i < testiH.size(); i++) {
            sb.append(getHyppy(i).XML());
            sb.append("\n");
        }
        sb.append("<Matto_Testi>\n");
        sb.append("\t<ID>\t" + "ID" + "\t</ID>\n");
        sb.append("\t<Etunimi>\t" + "Etunimi" + "\t</Etunimi>\n");
        sb.append("\t<Sukunimi>\t" + "Sukunimi" + "\t</Sukunimi>\n");
        sb.append("\t<Paivamaara>\t" + "Päivämäärä" + "\t</Paivamaara>\n");
        sb.append("\t<Stabiili_v>\t" + "Stabiili vasen" + "\t</Stabiili_v>\n");
        sb.append("\t<Stabiili_o>\t" + "Stabiili oikea" + "\t</Stabiili_o>\n");
        sb.append("\t<Esikevennetty_v>\t" + "Esikevennetty vasen" + "\t</Esikevennetty_v>\n");
        sb.append("\t<Esikevennetty_o>\t" + "Esikevennetty oikea" + "\t</Esikevennetty_o>\n");
        sb.append("\t<Stabiili_k>\t" + "Stabiili kahdella jalalla" + "\t</Stabiili_k>\n");
        sb.append("\t<Esikevennetty_k>\t" + "Esikevennetty kahdella jalalla" + "\t</Esikevennetty_k>\n");
        sb.append("</Matto_Testi>\n");
        sb.append("<Matto_Testi>\n");
        sb.append("\t<ID>\t" + "---" + "\t</ID>\n");
        sb.append("\t<Etunimi>\t" + "----------" + "\t</Etunimi>\n");
        sb.append("\t<Sukunimi>\t" + "-------------" + "\t</Sukunimi>\n");
        sb.append("\t<Paivamaara>\t" + "----------------" + "\t</Paivamaara>\n");
        sb.append("\t<Stabiili_v>\t" + "------------------" + "\t</Stabiili_v>\n");
        sb.append("\t<Stabiili_o>\t" + "------------------" + "\t</Stabiili_o>\n");
        sb.append("\t<Esikevennetty_v>\t" + "----------------------------" + "\t</Esikevennetty_v>\n");
        sb.append("\t<Esikevennetty_o>\t" + "----------------------------" + "\t</Esikevennetty_o>\n");
        sb.append("\t<Stabiili_k>\t" + "-----------------------------" + "\t</Stabiili_k>\n");
        sb.append("\t<Esikevennetty_k>\t" + "---------------------------------------" + "\t</Esikevennetty_k>\n");
        sb.append("</Matto_Testi>\n");
        for (int i = 0; i < testiM.size(); i++) {
            sb.append(getMatto(i).XML());
            sb.append("\n");
        }
        sb.append("<ViisKymppiViis_Testi>\n");
        sb.append("\t<ID>\t" + "ID" + "\t</ID>\n");
        sb.append("\t<Etunimi>\t" + "Etunimi" + "\t</Etunimi>\n");
        sb.append("\t<Sukunimi>\t" + "Sukunimi" + "\t</Sukunimi>\n");
        sb.append("\t<Paivamaara>\t" + "Päivämäärä" + "\t</Paivamaara>\n");
        sb.append("\t<Eka>\t" + "Eka" + "\t</Eka>\n");
        sb.append("\t<Toka>\t" + "Toka" + "\t</Toka>\n");
        sb.append("</ViisKymppiViis_Testi>\n");
        sb.append("<ViisKymppiViis_Testi>\n");
        sb.append("\t<ID>\t" + "---" + "\t</ID>\n");
        sb.append("\t<Etunimi>\t" + "----------" + "\t</Etunimi>\n");
        sb.append("\t<Sukunimi>\t" + "-------------" + "\t</Sukunimi>\n");
        sb.append("\t<Paivamaara>\t" + "----------------" + "\t</Paivamaara>\n");
        sb.append("\t<Eka>\t" + "-------" + "\t</Eka>\n");
        sb.append("\t<Toka>\t" + "-------" + "\t</Toka>\n");
        sb.append("</ViisKymppiViis_Testi>\n");
        for (int i = 0; i < testiV.size(); i++) {
            sb.append(getViis(i).XML());
            sb.append("\n");
        }
        sb.append("<Jaa_Testi>\n");
        sb.append("\t<ID>\t" + "ID" + "\t</ID>\n");
        sb.append("\t<Etunimi>\t" + "Etunimi" + "\t</Etunimi>\n");
        sb.append("\t<Sukunimi>\t" + "Sukunimi" + "\t</Sukunimi>\n");
        sb.append("\t<Paivamaara>\t" + "Päivämäärä" + "\t</Paivamaara>\n");
        sb.append("\t<JaaNopeus>\t" + "Jaa Nopeus" + "\t</JaaNopeus>\n");
        sb.append("\t<JaaCelssi>\t" + "Jaa Celssi" + "\t</JaaCelssi>\n");
        sb.append("</Jaa_Testi>\n");
        sb.append("<Jaa_Testi>\n");
        sb.append("\t<ID>\t" + "---" + "\t</ID>\n");
        sb.append("\t<Etunimi>\t" + "----------" + "\t</Etunimi>\n");
        sb.append("\t<Sukunimi>\t" + "-------------" + "\t</Sukunimi>\n");
        sb.append("\t<Paivamaara>\t" + "----------------" + "\t</Paivamaara>\n");
        sb.append("\t<JaaNopeus>\t" + "----------------" + "\t</JaaNopeus>\n");
        sb.append("\t<JaaCelssi>\t" + "--------------" + "\t</JaaCelssi>\n");
        sb.append("</Jaa_Testi>\n");
        for (int i = 0; i < testiJ.size(); i++) {
            sb.append(getJaa(i).XML());
            sb.append("\n");
        }
        sb.append("</testit>\n");
        return sb.toString();
    }

    public void openFile() throws IOException {

        outF = new PrintWriter(new FileOutputStream(outFile), true);

    }

    public void write(String row) {

        if (outF == null) {
            JOptionPane.showMessageDialog(null, "Ongelma tiedoston avauksessa!");
        } else {
            try {
                outF.println(row);
                convertXML2PDF(xml2, xsl, pdf2);
            } catch (IOException ex) {

            } catch (FOPException ex) {

            } catch (TransformerException ex) {

            }
        }
    }

    public void close() throws IOException, FOPException, TransformerException {
        outF.close();

    }

    public void teePDF() throws IOException, FOPException, TransformerException {
        XMLtoPDF app = new XMLtoPDF();
        app.convertXML2PDF(xml2, xsl, pdf2);
    }

    public void convertXML2PDF(File xml, File xslt, File pdf)
            throws IOException, FOPException, TransformerException {
        //Construct driver
        Driver driver = new Driver();

        //Setup logger
        Logger logger = new ConsoleLogger(ConsoleLogger.LEVEL_INFO);
        driver.setLogger(logger);
        MessageHandler.setScreenLogger(logger);

        //Setup Renderer (output format)
        driver.setRenderer(Driver.RENDER_PDF);
        //Setup output
        OutputStream out = new java.io.FileOutputStream(pdf);
        try {
            driver.setOutputStream(out);

            //Setup XSLT
            TransformerFactory factory = TransformerFactory.newInstance();

            Transformer transformer = factory.newTransformer(new StreamSource(xslt));
            //Setup input for XSLT transformation

            Source src = new StreamSource(xml);

            //Resulting SAX events (the generated FO) must be piped through to FOP
            Result res = new SAXResult(driver.getContentHandler());

            //Start XSLT transformation and FOP processing
            transformer.transform(src, res);
            JOptionPane.showMessageDialog(null, "PDF-tiedoston luonti onnistui!");
        } finally {
            out.close();
        }
    }

}
