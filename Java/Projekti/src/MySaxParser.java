
import java.io.*;
import javax.xml.parsers.*;

import org.xml.sax.*;
import org.xml.sax.helpers.*;

public class MySaxParser
        extends DefaultHandler {

    private static File outFile = new File("kaikki.xml");
    private static PrintWriter outF;
    // public static OutFile outF = new OutFile();
    public String st;
    private int testi;

    static String data;

    public static void main(String[] args) throws IOException {
        openFile();
        sax();

    }

    public static void openFile() throws IOException {
        if (outFile == null) {
            System.out.println("Tiedostonimi ei ole tiedossa.");
        } else {
            outF = new PrintWriter(new FileOutputStream(outFile), true); //näin avattua tiedostoa ei tarvitse sulkea
        }
    }

    public static void write(String row) {
        if (outF == null) {
            System.out.println("Virhe tiedoston kirjoituksessa.");
        } else {
            outF.println(row); // kirjoitetaan rivi
        }
    }

    public static void sax() {
        //String uri = args[0];
        String uri = "src\\kaikki.xml"; //http://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml     src\\Teht9\\Auto.xml
        try {
            SAXParserFactory parserFactory = SAXParserFactory.newInstance();
            parserFactory.setValidating(true);
            parserFactory.setNamespaceAware(false);
            parserFactory.setFeature(
                    "http://apache.org/xml/features/validation/schema", true);
            MySaxParser MySaxParserInstance = new MySaxParser();
            SAXParser parser = parserFactory.newSAXParser();
            parser.parse(uri, MySaxParserInstance);
        } catch (IOException exception) {
        } catch (SAXException exception) {
            System.out.println(exception.getMessage());
        } catch (ParserConfigurationException | FactoryConfigurationError exception) {
        }

    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {

        String s = new String(ch, start, length);

        //if (s.length()==0) // ei näytetä pelkkiä rivinvaihtoja
        //	return;
        try {
            if (testi == 0) {
                if (st.equals("ID")) {
                    data += s;
                } else if (st.equals("Etunimi")) {
                    data += s;
                } else if (st.equals("Sukunimi")) {
                    data += s;
                } else if (st.equals("Paivamaara")) {
                    data += s;
                } else if (st.equals("Aika22s")) {
                    data += s;
                } else if (st.equals("Aika25s")) {
                    data += s;
                } else if (st.equals("Aika29s")) {
                    data += s;
                }
            } else if (testi == 1) {
                if (st.equals("ID")) {
                    data += s;
                } else if (st.equals("Etunimi")) {
                    data += s;
                } else if (st.equals("Sukunimi")) {
                    data += s;
                } else if (st.equals("Paivamaara")) {
                    data += s;
                } else if (st.equals("Vauhditon")) {
                    data += s;
                } else if (st.equals("Viisloikka")) {
                    data += s;
                }

            } else if (testi == 2) {
                if (st.equals("ID")) {
                    data += s;
                } else if (st.equals("Etunimi")) {
                    data += s;
                } else if (st.equals("Sukunimi")) {
                    data += s;
                } else if (st.equals("Paivamaara")) {
                    data += s;
                } else if (st.equals("Stabiili_v")) {
                    data += s;
                } else if (st.equals("Stabiili_o")) {
                    data += s;
                } else if (st.equals("Esikevennetty_v")) {
                    data += s;
                } else if (st.equals("Esikevennetty_o")) {
                    data += s;
                } else if (st.equals("Stabiili_k")) {
                    data += s;
                } else if (st.equals("Esikevennetty_k")) {
                    data += s;
                }
            } else if (testi == 3) {
                if (st.equals("ID")) {
                    data += s;
                } else if (st.equals("Etunimi")) {
                    data += s;
                } else if (st.equals("Sukunimi")) {
                    data += s;
                } else if (st.equals("Paivamaara")) {
                    data += s;
                } else if (st.equals("Eka")) {
                    data += s;
                } else if (st.equals("Toka")) {
                    data += s;
                }

            } else if (testi == 4) {
                if (st.equals("ID")) {
                    data += s;
                } else if (st.equals("Etunimi")) {
                    data += s;
                } else if (st.equals("Sukunimi")) {
                    data += s;
                } else if (st.equals("Paivamaara")) {
                    data += s;
                } else if (st.equals("JaaNopeus")) {
                    data += s;
                } else if (st.equals("JaaCelssi")) {
                    data += s;
                }

            }
        } catch (Exception e) {

        }

    }

    /**
     * @throws org.xml.sax.SAXException
     * @todo Implement method "endDocument"
     */
    @Override
    public void endDocument() throws SAXException {

        System.out.println("Dokumentti loppuu.");

    }

    /**
     * @param url
     * @param localName
     * @param qName
     * @throws org.xml.sax.SAXException
     * @todo Implement method "endElement"
     */
    @Override
    public void endElement(String url, String localName, String qName) throws
            SAXException {

        System.out.println("Elementti loppuu: " + qName);
        if (null != qName) {
            switch (qName) {
                case "Celssin_Testi":
                    data += "</" + qName + ">\n";
                    write(data);
                    data = "";
                    break;
                case "Hyppy_Testi":
                    data += "</" + qName + ">\n";
                    write(data);
                    data = "";
                    break;
                case "Matto_Testi":
                    data += "</" + qName + ">\n";
                    write(data);
                    data = "";
                    break;
                case "ViisKymppiViis_Testi":
                    data += "</" + qName + ">\n";
                    write(data);
                    data = "";
                    break;
                case "Jaa_Testi":
                    data += "</" + qName + ">\n";
                    write(data);
                    data = "";
                    break;

                default:
                    data += "</" + qName + ">";

                    //write(data);
                    break;
            }
        }

        write(data);

    }

    /**
     * @throws org.xml.sax.SAXException
     * @todo Implement method "startDocument"
     */
    @Override
    public void startDocument() throws SAXException {

        System.out.println("Dokumentti alkaa.");
        write("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n");
        write("<testit>");
    }

    /**
     * @param uri
     * @param localName
     * @param qName
     * @param attributes
     * @throws org.xml.sax.SAXException
     * @todo Implement method "startElement"
     */
    @Override
    public void startElement(String uri, String localName, String qName,
            Attributes attributes) throws SAXException {
        //st = null;

        System.out.println("Elementti alkaa: " + qName);
        data = "<" + qName + ">";
        if (null != qName) {
            switch (qName) {
                case "Celssin_Testi":
                    // data += qName;
                    write(data);
                    break;
                case "Hyppy_Testi":
                    //data += qName;
                    write(data);
                    testi = 1;
                    break;
                case "Matto_Testi":
                    //data += qName;
                    write(data);
                    testi = 2;
                    break;
                case "ViisKymppiViis_Testi":
                    //data += qName;
                    write(data);
                    testi = 3;
                    break;
                case "Jaa_Testi":
                    //data += qName;
                    write(data);
                    testi = 4;
                    break;
                case "ID":
                    st = qName;
                    break;
                default:
                    st = qName;
                    break;
            }
        }
    }

    /**
     * @param e
     * @throws org.xml.sax.SAXException
     * @todo Implement method "fatalError"
     */
    @Override
    public void fatalError(SAXParseException e) throws SAXException {
        throw new java.lang.UnsupportedOperationException(
                "Method fatalError() not yet implemented.");
    }

}
