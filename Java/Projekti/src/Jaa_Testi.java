import javax.persistence.*;

@Entity
@Table(name ="jaa_testi")
public class Jaa_Testi {
    private int Numero,ID;
    private String Etunimi,Sukunimi,Paivamaara, JaaNopeus,JaaCelssi;

    public Jaa_Testi(int Numero, int ID, String Etunimi, String Sukunimi, String Paivamaara, String JaaNopeus, String JaaCelssi) {
        this.Numero = Numero;
        this.ID = ID;
        this.Etunimi = Etunimi;
        this.Sukunimi = Sukunimi;
        this.Paivamaara = Paivamaara;
        this.JaaNopeus = JaaNopeus;
        this.JaaCelssi = JaaCelssi;
    }

    public Jaa_Testi() {
    }
    
    @Id
    @GeneratedValue
    @Column(name = "Numero")
    public int getNumero() {
        return Numero;
    }

    public void setNumero(int Numero) {
        this.Numero = Numero;
    }
    @Column(name = "ID")
    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }
    @Column(name = "Etunimi")
    public String getEtunimi() {
        return Etunimi;
    }

    public void setEtunimi(String Etunimi) {
        this.Etunimi = Etunimi;
    }
    @Column(name = "Sukunimi")
    public String getSukunimi() {
        return Sukunimi;
    }

    public void setSukunimi(String Sukunimi) {
        this.Sukunimi = Sukunimi;
    }
    @Column(name = "Paivamaara")
    public String getPaivamaara() {
        return Paivamaara;
    }

    public void setPaivamaara(String Paivamaara) {
        this.Paivamaara = Paivamaara;
    }
    @Column(name = "JaaNopeus")
    public String getJaaNopeus() {
        return JaaNopeus;
    }

    public void setJaaNopeus(String JaaNopeus) {
        this.JaaNopeus = JaaNopeus;
    }
    @Column(name = "JaaCelssi")
    public String getJaaCelssi() {
        return JaaCelssi;
    }

    public void setJaaCelssi(String JaaCelssi) {
        this.JaaCelssi = JaaCelssi;
    }
    public String XML() {
        StringBuffer sb = new StringBuffer();
        sb.append("<Jaa_Testi>\n");
        sb.append("\t<ID>\t" + ID + "\t</ID>\n");
        sb.append("\t<Etunimi>\t" + Etunimi + "\t</Etunimi>\n");
        sb.append("\t<Sukunimi>\t" + Sukunimi + "\t</Sukunimi>\n");
        sb.append("\t<Paivamaara>\t" + Paivamaara + "\t</Paivamaara>\n");
        sb.append("\t<JaaNopeus>\t" + JaaNopeus + "\t</JaaNopeus>\n");
        sb.append("\t<JaaCelssi>\t" + JaaCelssi + "\t</JaaCelssi>\n");
        sb.append("</Jaa_Testi>\n");
        return sb.toString();
    }
    
    
    
}
