
import javax.persistence.*;

@Entity
@Table(name = "celssin_testi")
public class Celssin_Testi {
    private int Numero,ID;
    private String Etunimi,Sukunimi,Paivamaara, Aika22s,Aika25s,Aika29s;

    public Celssin_Testi(int Numero, int ID, String Etunimi, String Sukunimi, String Paivamaara, String Aika22s, String Aika25s, String Aika29s) {
        this.Numero = Numero;
        this.ID = ID;
        this.Etunimi = Etunimi;
        this.Sukunimi = Sukunimi;
        this.Paivamaara = Paivamaara;
        this.Aika22s = Aika22s;
        this.Aika25s = Aika25s;
        this.Aika29s = Aika29s;
    }

    public Celssin_Testi() {
    }
    @Id
    @GeneratedValue
    @Column(name = "Numero")
    public int getNumero() {
        return Numero;
    }

    public void setNumero(int Numero) {
        this.Numero = Numero;
    }
    @Column(name = "ID")
    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }
    @Column(name = "Etunimi")
    public String getEtunimi() {
        return Etunimi;
    }

    public void setEtunimi(String Etunimi) {
        this.Etunimi = Etunimi;
    }
    @Column(name = "Sukunimi")
    public String getSukunimi() {
        return Sukunimi;
    }

    public void setSukunimi(String Sukunimi) {
        this.Sukunimi = Sukunimi;
    }
    @Column(name = "Paivamaara")
    public String getPaivamaara() {
        return Paivamaara;
    }

    public void setPaivamaara(String Paivamaara) {
        this.Paivamaara = Paivamaara;
    }
    @Column(name = "Aika22s")
    public String getAika22s() {
        return Aika22s;
    }

    public void setAika22s(String Aika22s) {
        this.Aika22s = Aika22s;
    }
    @Column(name = "Aika25s")
    public String getAika25s() {
        return Aika25s;
    }

    public void setAika25s(String Aika25s) {
        this.Aika25s = Aika25s;
    }
    @Column(name = "Aika29s")
    public String getAika29s() {
        return Aika29s;
    }

    public void setAika29s(String Aika29s) {
        this.Aika29s = Aika29s;
    }
    
    public String XML() {
        StringBuffer sb = new StringBuffer();
        sb.append("<Celssin_Testi>\n");
        sb.append("\t<ID>\t" + ID + "\t</ID>\n");
        sb.append("\t<Etunimi>\t" + Etunimi + "\t</Etunimi>\n");
        sb.append("\t<Sukunimi>\t" + Sukunimi + "\t</Sukunimi>\n");
        sb.append("\t<Paivamaara>\t" + Paivamaara + "\t</Paivamaara>\n");
        sb.append("\t<Aika22s>\t" + Aika22s + "\t</Aika22s>\n");
        sb.append("\t<Aika25s>\t" + Aika25s + "\t</Aika25s>\n");
        sb.append("\t<Aika29s>\t" + Aika29s + "\t</Aika29s>\n");
        sb.append("</Celssin_Testi>\n");
        return sb.toString();
    }
}
