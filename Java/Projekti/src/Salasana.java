    
import java.security.Key;
import java.util.Iterator;
import java.util.List;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import org.hibernate.Session;
import org.hibernate.Transaction;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;




public class Salasana {

    private static final String ALGORITHM = "AES";
    private static final String KEY = "1Hbfh667adfDEJ78";
    private boolean makkara = false;
    
 
    
    public static String encrypt(String value) throws Exception
    {
        Key key = generateKey();
        Cipher cipher = Cipher.getInstance(Salasana.ALGORITHM);
        cipher.init(Cipher.ENCRYPT_MODE, key);
        byte [] encryptedByteValue = cipher.doFinal(value.getBytes("utf-8"));
        String encryptedValue64 = new BASE64Encoder().encode(encryptedByteValue);
        return encryptedValue64;
               
    }
    
    public static String decrypt(String value) throws Exception
    {
        Key key = generateKey();
        Cipher cipher = Cipher.getInstance(Salasana.ALGORITHM);
        cipher.init(Cipher.DECRYPT_MODE, key);
        byte [] decryptedValue64 = new BASE64Decoder().decodeBuffer(value);
        byte [] decryptedByteValue = cipher.doFinal(decryptedValue64);
        String decryptedValue = new String(decryptedByteValue,"utf-8");
        return decryptedValue;
                
    }
    
    private static Key generateKey() throws Exception 
    {
        Key key = new SecretKeySpec(Salasana.KEY.getBytes(),Salasana.ALGORITHM);
        return key;
    }
    
    public boolean haeSalasana(String tunnus, String salasana){
        
        Session istunto = dBConnection.getIstuntotehdas().openSession();
        Transaction transaktio = null;
        List lista = null;
        try {
            transaktio = istunto.beginTransaction();
            lista = istunto.createQuery("FROM Kayttjat_t where Tunnus = "+"'"+tunnus+"'").list();
            for (Iterator it = lista.iterator(); it.hasNext();) {
                Kayttjat_t kayttaja = (Kayttjat_t) it.next();
                if(salasana.equals(kayttaja.getSalasana())){
                    makkara = true;
                }else{
                    makkara = false;
                }
            }
            
            
        }catch(Exception e){
            
        }finally {
                istunto.close();
            }
        return makkara;
    }
    
    
    
    
    
}