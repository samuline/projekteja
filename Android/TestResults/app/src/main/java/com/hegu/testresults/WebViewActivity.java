package com.hegu.testresults;


    import android.app.Activity;
    import android.os.Bundle;
    import android.webkit.WebView;
    import android.webkit.WebViewClient;

public class WebViewActivity extends Activity {

        private WebView webview;

        public void onCreate(Bundle savedInstanceState){
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_webview);

            webview = (WebView)findViewById(R.id.webView1);
            webview.setWebViewClient(new MyWebViewClient());
            String url = "http://82.181.21.139"; 
            webview.getSettings().setJavaScriptEnabled(true);
            webview.loadUrl(url);


        }


    private class MyWebViewClient extends WebViewClient {
            public boolean shouldOverrideUrlLoading(WebView view, String url){
                view.loadUrl(url);
                return true;
            }
        }

    }

